const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const userRoute = require("./routes/user");
const regRoute = require("./routes/register");
const loginRoute = require("./routes/login");
const productRoute = require("./routes/product");
const cartRoute = require("./routes/cart");
const orderRoute = require("./routes/order");
const stripeRoute = require("./routes/stripe");
const cors = require("cors");

dotenv.config();

mongoose
  .connect(process.env.MONGO_URL)
  .then(() => console.log("API connected to MongoDB"))
  .catch((err) => {
    console.log(err);
  });

const corsOptions = {
  origin: "https://aqueous-inlet-78462.herokuapp.com/",
  credentials: true,
  optionSuccessStatus: 200,
  allowedHeaders: "*",
  allowMethods: "*",
};

app.use(cors(corsOptions));
app.use(express.json());
app.use("/users", userRoute);
app.use("/register", regRoute);
app.use("/login", loginRoute);
app.use("/products", productRoute);
app.use("/cart", cartRoute);
app.use("/orders", orderRoute);
app.use("/checkout", stripeRoute);

app.listen(process.env.PORT || 4000, () => {
  console.log(`API is now online`);
});

const router = require("express").Router();
const User = require("../models/User");
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");

// Login

router.post("/", async (req, res) => {
  try {
    const user = await User.findOne({
      username: req.body.username,
    });

    // Condition if there's no user
    !user && res.status(401).json("Wrong credentials!");

    // Decrypting password
    const hashedPassword = CryptoJS.AES.decrypt(
      user.password,
      process.env.SEC_PASS
    );
    // Turning decrypted password into string
    const OriginalPassword = hashedPassword.toString(CryptoJS.enc.Utf8);

    // Condition if there's no/incorrect password
    OriginalPassword !== req.body.password &&
      res.status(401).json("Wrong credentials!");

    // To return user info aside from the password
    const { password, ...others } = user._doc;

    // Creating JWT token
    const accessToken = jwt.sign(
      {
        id: user._id,
        isAdmin: user.isAdmin,
      },
      process.env.JWT_SEC,
      { expiresIn: "30d" }
    );

    // Return if successful
    res.status(200).json({ ...others, accessToken });
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router;
